# ChaosTaskFront

Prerequisites:
* node, npm
* angular cli tool (npm i -g @angular/cli)

1) Clone the repo

2) Install dependencies
    ```bash
    npm install
    ```
3) Run
    ```bash
    ng serve
    ```