import { environment } from '../environments/environment';

export class Constants {
  public static readonly API_URL_PREFIX = '/api';
  public static readonly API_URL = environment.apiUrl;
  public static readonly MAPBOX_API_KEY = 'pk.eyJ1Ijoia29mbHlhbiIsImEiOiJjamdldzg0a3kwNDgzMnhxc2h6NnlxdDhiIn0.WyuEFDEfhxkSyaMbp-wxdw';
}
