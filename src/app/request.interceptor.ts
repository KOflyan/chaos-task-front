import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Constants } from '../constants/constants';

@Injectable()
export class HttpInterceptorImpl implements HttpInterceptor {

  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    if (req.url.startsWith('/assets')) {
      return next.handle(req);
    }

    const apiReq = req.clone({
      url: `${Constants.API_URL}${Constants.API_URL_PREFIX}${req.url}`,
      headers: new HttpHeaders({ 'Content-Type': 'Application/json' })
    });

    if (req.url.startsWith('/passwordReset/confirm')) {
      return next.handle(apiReq);
    }

    return next.handle(apiReq);
  }
}
