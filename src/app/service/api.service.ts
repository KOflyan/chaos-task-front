import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TotalRecords } from '../model/total-records.model';
import { ProfessionalArea } from '../model/professional-area.model';
import { Organisation } from '../model/organisation.model';
import { Offer } from '../model/offer.model';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private http: HttpClient
  ) {}

  public getTotalRecords(): Observable<TotalRecords> {
    return this.http.get<TotalRecords>('/stats/total_records');
  }

  public getProfessionalAreas(): Observable<ProfessionalArea[]> {
    return this.http.get<ProfessionalArea[]>('/stats/offers_by_profession_areas');
  }

  public getOrganisations(): Observable<Organisation[]> {
    return this.http.get<Organisation[]>('/stats/offers_by_organisations');
  }

  public getOffers(): Observable<Offer[]> {
    return this.http.get<Offer[]>('/stats/offers');
  }
}
