export interface Organisation {
  org_id: number;
  organisation: string;
  offers: number;
}
