export interface ProfessionalArea {
  area_id: number;
  area: string;
  offers: number;
}
