import { Offer } from './offer.model';
import { TotalRecords } from './total-records.model';
import { Organisation } from './organisation.model';
import { ProfessionalArea } from './professional-area.model';

export interface Data {
  offers: Offer[];
  organisations: Organisation[];
  areas: ProfessionalArea[];
  total: TotalRecords;
}
