export interface Offer {
  id: number;
  area: string;
  organisation: string;
  task_description: string;
  offer_link: string;
  address: string;
  offer_expires: string;
  lat: number;
  long: number;
}
