export interface TotalRecords {
  offers: number;
  organisations: number;
  professional_areas: number;
}
