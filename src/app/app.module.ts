import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ChartsModule } from 'ng2-charts';
import { AppComponent } from './component/app.component';
import { BarChartComponent } from './component/bar-chart/bar-chart.component';
import { PieChartComponent } from './component/pie-chart/pie-chart.component';
import { ApiService } from './service/api.service';
import { HttpInterceptorImpl } from './request.interceptor';
import { NgxSpinnerModule } from 'ngx-spinner';
import { MapComponent } from './component/map/map.component';

@NgModule({
  declarations: [
    AppComponent,
    BarChartComponent,
    PieChartComponent,
    MapComponent
  ],
  imports: [
    ChartsModule,
    BrowserAnimationsModule,
    BrowserModule,
    NgxSpinnerModule,
    HttpClientModule
  ],
  providers: [
    ApiService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorImpl,
      multi: true,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
