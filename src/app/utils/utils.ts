export class Utils {

  public static capitalize(e: string): string {
    if (!e || !e.length) { return e; }
    return e[0].toUpperCase() + e.substr(1);
  }
}
