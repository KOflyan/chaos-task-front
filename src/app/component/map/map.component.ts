import { Component, OnInit, Input } from '@angular/core';
import * as mapboxgl from 'mapbox-gl';
import { Constants } from '../../../constants/constants';
import { Offer } from '../../model/offer.model';
@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
})
export class MapComponent implements OnInit {
  @Input() offers: Offer[] = [];

  public map: mapboxgl.Map;
  public style = 'mapbox://styles/mapbox/dark-v10';
  public lat = 37.75;
  public lng = -122.41;

  constructor() {}

  ngOnInit(): void {
    (mapboxgl as any).accessToken = Constants.MAPBOX_API_KEY;
    this.map = new mapboxgl.Map({
      container: 'map',
      style: this.style,
      zoom: 10,
      center: [24.935559069040664, 60.31707122961028],
    });
    this.map.addControl(new mapboxgl.NavigationControl());
    this._addPoints();
  }

  private _addPoints(): void {
    const geojson = {
      type: 'geojson',
      data: {
        type: 'FeatureCollection',
        features: [],
      },
    };

    this.offers.forEach((o) => {
      const popup = new mapboxgl.Popup({ offset: 25 })
        .setLngLat([o.lat, o.long])
        .setHTML(`
        <div>
          <span class="label">Professional Area</span>
          ${o.area}
        <div>
          <span class="label">Address</span>
          ${o.address}
        </div>
        <div>
          <span class="label">Description</span>
          ${o.task_description}
        </div>
        <div>
          <span class="label">Expires</span>
          ${o.offer_expires}
        </div>
        <div>
          <span class="label">Link</span>
          <a target="_blank" href="${o.offer_link}">${o.offer_link}</a>
        </div>`)
        .addTo(this.map);

      new mapboxgl.Marker()
        .setPopup(popup)
        .setLngLat([o.lat, o.long]).addTo(this.map);
    });
  }
}
