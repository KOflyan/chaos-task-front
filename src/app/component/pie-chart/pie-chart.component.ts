import { Component, OnInit, Input } from '@angular/core';
import { ChartOptions, ChartType } from 'chart.js';
import { Label, SingleDataSet } from 'ng2-charts';
import { TotalRecords } from '../../model/total-records.model';
import { Utils } from '../../utils/utils';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';


@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html'
})
export class PieChartComponent implements OnInit {

  public readonly pieChartOptions: ChartOptions = {
    responsive: true,
    plugins: {
      datalabels: {
        color: 'white'
      }
    }
  };
  public readonly pieChartType: ChartType = 'pie';

  @Input() rawData: TotalRecords;

  public chartColors: Array<{ backgroundColor: string[] }> = [
    {
      backgroundColor: ['red', '#b000b5', 'green']
    }
  ];
  public labels: Label[] = [];
  public data: SingleDataSet = [];
  public pieChartLegend = true;
  public pieChartPlugins = [pluginDataLabels];

  constructor() {}

  ngOnInit(): void {
    this.data = Object.values(this.rawData);
    this.labels = Object.keys(this.rawData)
      .map(e => Utils.capitalize(e.split('_').join(' ')));
  }
}
