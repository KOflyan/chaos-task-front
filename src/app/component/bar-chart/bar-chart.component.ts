import { Component, OnInit, Input } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Label } from 'ng2-charts';
import { ProfessionalArea } from '../../model/professional-area.model';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { Utils } from '../../utils/utils';

@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html'
})
export class BarChartComponent implements OnInit {

  public readonly barChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'top',
    },
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };

  @Input() public barChartLabels: Label[] = [];
  @Input() public barChartData: ChartDataSets[] = [];
  @Input() public color = 'purple';
  @Input() public barChartType: ChartType = 'bar';


  public chartColors: Array<{ backgroundColor: string[] }> = [
    {
      backgroundColor: []
    }
  ];
  public barChartLegend = true;
  public barChartPlugins = [pluginDataLabels];


  ngOnInit() {
    this.barChartLabels.forEach(l => this.chartColors[0].backgroundColor.push(this.color));
    this.chartColors[0].backgroundColor.push(this.color);
  }
}
