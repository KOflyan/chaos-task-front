import { Component, OnInit } from '@angular/core';
import { Observable, forkJoin } from 'rxjs';
import { delay } from 'rxjs/operators';
import { ApiService } from '../service/api.service';
import { Offer } from '../model/offer.model';
import { Organisation } from '../model/organisation.model';
import { ProfessionalArea } from '../model/professional-area.model';
import { TotalRecords } from '../model/total-records.model';
import { Data } from '../model/data.model';
import { NgxSpinnerService } from 'ngx-spinner';
import { ChartDataSets, ChartColor } from 'chart.js';
import { Utils } from '../utils/utils';
import { Label } from 'ng2-charts';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
  title = 'chaos-task-front';

  public data: Data;
  public areasChartLabels: Label[] = [];
  public areasChartData: ChartDataSets[] = [];
  public areasChartColors: ChartDataSets[] = [];

  public organisaionsChartLabels: Label[] = [];
  public organisaionsChartData: ChartDataSets[] = [];


  constructor(
    private spinner: NgxSpinnerService,
    private apiService: ApiService
  ) {}

  ngOnInit() {
    this.spinner.show();

    forkJoin([
      this.organisations$,
      this.professionalAreas$,
      this.offers$,
      this.total$
    ])
    .pipe(delay(1000))
    .subscribe(([organisations, areas, offers, total]) => {
      this.data = { organisations, areas, offers, total };
      this.spinner.hide();
      this._composeBarChartsData(this.data.areas, this.data.organisations);
    }, (err) => {
      console.error(err);
      this.spinner.hide();
    });
  }

  get offers$(): Observable<Offer[]> {
    return this.apiService.getOffers();
  }

  get organisations$(): Observable<Organisation[]> {
    return this.apiService.getOrganisations();
  }

  get professionalAreas$(): Observable<ProfessionalArea[]> {
    return this.apiService.getProfessionalAreas();
  }

  get total$(): Observable<TotalRecords> {
    return this.apiService.getTotalRecords();
  }

  private _composeBarChartsData(areas: ProfessionalArea[] = [], organisations: Organisation[] = []): void {
    let d = { data: [], label: 'Job offers by professional areas' };
    areas.forEach(a => {
      this.areasChartLabels.push(Utils.capitalize(a.area));
      d.data.push(a.offers);
    });

    this.areasChartData.push(d);
    d = { data: [], label: 'Job offers by organisations' };
    organisations.forEach(o => {
      o.organisation = o.organisation.length > 200 ? o.organisation.substr(0, 80) + '...' : o.organisation;
      this.organisaionsChartLabels.push(Utils.capitalize(o.organisation));
      d.data.push(o.offers);
    });
    this.organisaionsChartData.push(d);
  }
}
